package myad.Publiccc;

import myad.Database.Database_Helper;
import myad.Model.BookModel;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.Data;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by bill on 2016-01-20.
 */
@WebServlet("/book/view")
public class ViewBookServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        process(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
       process(request,response);
    }

    public static void process(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        if(request.getParameter("t")!=null)
        {
            Database_Helper database_helper = new Database_Helper();
            BookModel book = new BookModel();
            try
            {
                book = database_helper.viewBook(request.getParameter("t"));//passing title getting results
                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
                if(book.getId()!=0)
                {
                    request.setAttribute("book",book);
                    RequestDispatcher requestDispatcher = request.getRequestDispatcher("/book/view/index.jsp");
                    requestDispatcher.include(request,response);
                    System.out.print("i have  id");
                }
                else {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                    System.out.print("i do not have  id");
                }

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        else {
            response.sendRedirect("/book/");
        }
    }
}
