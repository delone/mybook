package myad.Publiccc;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Created by bill on 2016-01-20.
 */
@WebServlet("/view")
public class ViewHomeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if(request.getParameter("type") !=null && request.getParameter("t") != null)
        {
            if(request.getParameter("type")=="b")
            {
                ViewBookServlet.process(request,response);
            }
            else if(request.getParameter("type")=="j")
            {
                ViewJournalServlet.process(request,response);
            }
            else
            {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                System.out.print("i ama not found");
            }
        }
        else
        {
//            response.sendError(HttpServletResponse.SC_NOT_FOUND);System.out.print("i ama not found");
            System.out.print(request.getParameter("t"));

        }
    }
}
