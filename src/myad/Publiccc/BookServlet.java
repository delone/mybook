package myad.Publiccc;

import myad.Database.Database_Helper;
import myad.Model.BaseModel;
import myad.Model.BookModel;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by bill on 2016-01-13.
 */
@WebServlet("/book/")
public class BookServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Database_Helper database_helper = new Database_Helper();
        String query_count = " select count(*) from books";
        Integer items_per_page = Integer.parseInt(getServletContext().getInitParameter("items_per_page"));// i get it from web.xml
        Integer currentPage = 1;//by default
        Integer numberOfPages = 1;
        if (request.getParameter("page")==null)
        {
            currentPage = 1;
        }
        else
        {
            currentPage = Integer.parseInt(request.getParameter("page"));
        }
        try {
            Double totalRowCount = (double) database_helper.getTotalAll(query_count);// I get total number of pages
            numberOfPages = (int) Math.ceil(totalRowCount/items_per_page);///how many pages I have
            List<BookModel> books = database_helper.getBooks((currentPage-1)*items_per_page,(currentPage-1)*items_per_page+items_per_page);
            request.setAttribute("books",books);
            request.setAttribute("numberOfPages",numberOfPages);
            request.setAttribute("currentPage",currentPage);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/book/index.jsp");
            requestDispatcher.include(request,response );
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
