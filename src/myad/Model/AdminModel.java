package myad.Model;

/**
 * Created by bill on 2016-01-19.
 */
public class AdminModel
{
    private Integer Id;
    private String Password;
    private String Name;
    private String Surname;
    private String Email;
    private String Username;

    @Override
    public String toString() {
        return "AdminModel{" +
                "Id=" + Id +
                ", Password='" + Password + '\'' +
                ", Name='" + Name + '\'' +
                ", Surname='" + Surname + '\'' +
                ", Email='" + Email + '\'' +
                ", Username='" + Username + '\'' +
                '}';
    }

    public AdminModel() {
        Id = 0;
        Password = "";
        Name = "";
        Surname = "";
        Email = "";
        Username = "";
    }

    public Integer getId() {

        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }
}
