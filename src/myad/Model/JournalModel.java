package myad.Model;

/**
 * Created by bill on 2016-01-19.
 */
public class JournalModel extends BaseModel
{
    private String Publisher;
    private String Release_Date;

    public String getJ() {
        return j;
    }

    public void setJ(String j) {
        this.j = j;
    }

    private String PageCount;
    private String Genre;
    private String View;
    private String Like;
    private String Edition;
    private String j;

    @Override
    public String toString() {
        return "JournalModel{" +
                "Publisher='" + Publisher + '\'' +
                ", Release_Date='" + Release_Date + '\'' +
                ", PageCount='" + PageCount + '\'' +
                ", Genre='" + Genre + '\'' +
                ", View='" + View + '\'' +
                ", Like='" + Like + '\'' +
                ", Edition='" + Edition + '\'' +
                ", j='" + j + '\'' +
                '}';
    }

    public JournalModel() {
        this.Id = 0;
        this.Title = "";
        this.Description = "";
        this.Publisher = "";
        this.Image = "";
        this.Release_Date = "";
        this.PageCount = "";
        this.Genre = "";
        this.View = "";
        this.Like = "";
        this.Edition = "";
        this.j = "";
    }



    public String getPublisher() {
        return Publisher;
    }


    public String getRelease_Date() {
        return Release_Date;
    }

    public String getPageCount() {
        return PageCount;
    }

    public String getGenre() {
        return Genre;
    }

    public String getView() {
        return View;
    }

    public String getLike() {
        return Like;
    }

    public String getEdition() {
        return Edition;
    }



    public void setPublisher(String publisher) {
        Publisher = publisher;
    }

    public void setRelease_Date(String release_Date) {
        Release_Date = release_Date;
    }

    public void setPageCount(String pageCount) {
        PageCount = pageCount;
    }

    public void setGenre(String genre) {
        Genre = genre;
    }

    public void setView(String view) {
        View = view;
    }

    public void setLike(String like) {
        Like = like;
    }

    public void setEdition(String edition) {
        Edition = edition;
    }
}
