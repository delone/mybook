package myad.Model;

/**
 * Created by bill on 2016-01-19.
 */
public class CommentModel
{
    private Integer Id;
    private String Name;
    private String Surname;

    @Override
    public String toString() {
        return "CommentModel{" +
                "Id=" + Id +
                ", Name='" + Name + '\'' +
                ", Surname='" + Surname + '\'' +
                ", Email='" + Email + '\'' +
                '}';
    }

    public CommentModel() {
        Id = 0;
        Name = "";
        Surname = "";
        Email = "";
    }

    public String getEmail() {

        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    private String Email;
}
