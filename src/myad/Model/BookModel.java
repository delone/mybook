package myad.Model;

import java.io.Serializable;

/**
 * Created by bill on 2016-01-18.
 */
public class BookModel extends BaseModel
{
    private String Author;
    private String Publisher;
    private String Release_Date;
    private String PageCount;
    private String Genre;
    private String View;
    private String Like;
    private String Edition;
    private String b;

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "BookModel{" +
                "Author='" + Author + '\'' +
                ", Publisher='" + Publisher + '\'' +
                ", Release_Date='" + Release_Date + '\'' +
                ", PageCount='" + PageCount + '\'' +
                ", Genre='" + Genre + '\'' +
                ", View='" + View + '\'' +
                ", Like='" + Like + '\'' +
                ", Edition='" + Edition + '\'' +
                ", b='" + b + '\'' +
                '}';
    }

    public BookModel()
    {
        this.Id = 0;
        this.Title = "";
        this.Description = "";
        this.Author = "";
        this.Publisher = "";
        this.Image = "";
        this.Release_Date = "";
        this.PageCount = "";
        this.Genre = "";
        this.View = "";
        this.Like = "";
        this.Edition = "";
        this.b = "";
    }


    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getPublisher() {
        return Publisher;
    }

    public void setPublisher(String publisher) {
        Publisher = publisher;
    }



    public String getRelease_Date() {
        return Release_Date;
    }

    public void setRelease_Date(String release_Date) {
        Release_Date = release_Date;
    }

    public String getPageCount() {
        return PageCount;
    }

    public void setPageCount(String pageCount) {
        PageCount = pageCount;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String genre) {
        Genre = genre;
    }

    public String getView() {
        return View;
    }

    public void setView(String view) {
        View = view;
    }

    public String getLike() {
        return Like;
    }

    public void setLike(String like) {
        Like = like;
    }

    public String getEdition() {
        return Edition;
    }

    public void setEdition(String edition) {
        Edition = edition;
    }

}
