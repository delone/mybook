package myad.Model;

/**
 * Created by bill on 2016-01-19.
 */
public class BaseModel
{
    protected Integer Id;
    protected String Title;
    protected String Description;
    protected String Image;
    @Override
    public String toString() {
        return "BaseModel{" +
                "Id=" + Id +
                ", Title='" + Title + '\'' +
                ", Description='" + Description + '\'' +
                ", Image='" + Image + '\'' +
                '}';
    }

    public BaseModel() {
        Id = 0;
        Title = "";
        Description = "";
        Image = "";
    }

    public String getImage() {

        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }


}
