package myad.Database;

import myad.Model.BaseModel;
import myad.Model.BookModel;
import myad.Model.JournalModel;

import java.awt.print.Book;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bill on 2016-01-15.
 */
public class Database_Helper
{
    public  List<BookModel> getBooks(int start,int end) throws SQLException
    {
        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;
        List<BookModel> books = new ArrayList<BookModel>();
        String query_book = "select * from (select ROWNUM r ,id,TITLE,DESCRIPTION,AUTHOR,PUBLISHER,IMAGE,RELEASE_DATE,\n" +
                "                 PAGECOUNT,GENRE,VIEW_COUNT,SUBSCRIPTION_COUNT,EDITION from (\n" +
                "  select * from books)\n" +
                ") where r BETWEEN "+String.valueOf(start)+" and "+String.valueOf(end) +"";
        try
        {
            connection = Db_Connect.connect();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query_book);
            while (resultSet.next())
            {
                BookModel book = new BookModel();
                book.setId(resultSet.getInt("id"));
                book.setTitle(resultSet.getString("title"));
                book.setDescription(resultSet.getString("description"));
                book.setAuthor(resultSet.getString("author"));
                book.setPublisher(resultSet.getString("publisher"));
                book.setImage(resultSet.getString("image"));
                book.setRelease_Date(resultSet.getString("release_date"));
                book.setPageCount(resultSet.getString("pagecount"));
                book.setGenre(resultSet.getString("genre"));
                book.setView(resultSet.getString("view_count"));
                book.setLike(resultSet.getString("subscription_count"));
                book.setEdition(resultSet.getString("edition"));
                books.add(book);
            }
        }
        catch(Exception ex)
        {
            System.out.println("Error while getting book list " + ex.getMessage());
            ex.printStackTrace();
        }
        finally {
            JdbcUtil.close(resultSet,statement,connection);
        }
       return books;
    }

    public  List<JournalModel> getJournals(int start,int end) throws SQLException
    {
        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;
        List<JournalModel> journals = new ArrayList<JournalModel>();
        String query_journal = "select * from (select ROWNUM r ,id,TITLE,DESCRIPTION,PUBLISHER,IMAGE,RELEASE_DATE,\n" +
                "                 PAGECOUNT,GENRE,VIEW_COUNT,SUBSCRIPTION_COUNT,EDITION from (\n" +
                "  select * from journals)\n" +
                ") where r BETWEEN "+String.valueOf(start)+" and "+String.valueOf(end) +"";
        try
        {
            connection = Db_Connect.connect();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query_journal);
            while (resultSet.next())
            {
                JournalModel journal = new JournalModel();
                journal.setId(resultSet.getInt("id"));
                journal.setTitle(resultSet.getString("title"));
                journal.setDescription(resultSet.getString("description"));
                journal.setPublisher(resultSet.getString("publisher"));
                journal.setImage(resultSet.getString("image"));
                journal.setRelease_Date(resultSet.getString("release_date"));
                journal.setPageCount(resultSet.getString("pagecount"));
                journal.setGenre(resultSet.getString("genre"));
                journal.setView(resultSet.getString("view_count"));
                journal.setLike(resultSet.getString("subscription_count"));
                journal.setEdition(resultSet.getString("edition"));
                journals.add(journal);
            }
        }
        catch(Exception ex)
        {
            System.out.println("Error while getting book list " + ex.getMessage());
            ex.printStackTrace();
        }
        finally {
            JdbcUtil.close(resultSet,statement,connection);
        }
        return journals;
    }

    public  List<BaseModel> getAll(int start,int end) throws SQLException
    {

        Integer totalpages;
        String query = "select * from (\n" +
                "\n" +
                "  select rownum r,id,title,image,DESCRIPTION from(\n" +
                "    select id,title,image,DESCRIPTION from\n" +
                "    journals union all select id,title,image,DESCRIPTION from books\n" +
                "  )\n" +
                "\n" +
                ") where r BETWEEN "+String.valueOf(start)+" and "+String.valueOf(end) +"";
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<BaseModel> allitems = new ArrayList<BaseModel>();
        try
        {
            connection = Db_Connect.connect();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next())
            {
                BaseModel baseModel = new BaseModel();
                baseModel.setId(resultSet.getInt("id"));
                baseModel.setTitle(resultSet.getString("title"));
                baseModel.setDescription(resultSet.getString("description"));
                baseModel.setImage(resultSet.getString("image"));
                allitems.add(baseModel);
            }
        }
        catch (Exception ex)
        {
            System.out.print("Error while getting basedomain stuff " + ex.getMessage());
            ex.printStackTrace();
        }
        finally {
            JdbcUtil.close(resultSet,statement,connection);
        }
        return allitems;
    }

    public  Integer delete_Book(int id) throws SQLException
    {
        String query_delete_book = "DELETE from books where id =?";
        Connection connection = Db_Connect.connect();
        PreparedStatement preparedStatement = connection.prepareStatement(query_delete_book);
        preparedStatement.setInt(1,id);
        Integer result = preparedStatement.executeUpdate();
        return result;
    }

    public  Integer delete_Journal(int id) throws SQLException
    {
        String query_delete_journal = "DELETE from journals where id =?";
        Connection connection = Db_Connect.connect();
        PreparedStatement preparedStatement = connection.prepareStatement(query_delete_journal);
        preparedStatement.setInt(1,id);
        Integer result = preparedStatement.executeUpdate();
        return result;
    }

    public Integer getTotalAll(String query) throws SQLException
    {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        Integer count = 0;
        try
        {
            connection = Db_Connect.connect();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next())
            {
                count = resultSet.getInt(1);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally {
            JdbcUtil.close(resultSet,statement,connection);
        }
        return count;
    }

    public BookModel viewBook(String title) throws SQLException
    {
        String query = "select * from books where title=?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        BookModel book = new BookModel();
        try
        {
            connection = Db_Connect.connect();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,title);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next())
            {
                book.setId(resultSet.getInt("id"));
                book.setTitle(resultSet.getString("title"));
                book.setDescription(resultSet.getString("description"));
                book.setAuthor(resultSet.getString("author"));
                book.setPublisher(resultSet.getString("publisher"));
                book.setImage(resultSet.getString("image"));
                book.setRelease_Date(resultSet.getString("release_date"));
                book.setPageCount(resultSet.getString("pagecount"));
                book.setGenre(resultSet.getString("genre"));
                book.setView(resultSet.getString("view_count"));
                book.setLike(resultSet.getString("subscription_count"));
                book.setEdition(resultSet.getString("edition"));

            }
        }
        catch (Exception ex)
        {
            System.out.println("Error while viewing book" + ex.getMessage());
            ex.printStackTrace();
        }
        finally {
            JdbcUtil.close(resultSet,preparedStatement,connection);
        }
        return book;
    }

    public JournalModel viewJournal(String title) throws SQLException
    {
        String query = "select * from journals where title=?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        JournalModel journal = new JournalModel();
        try
        {
            connection = Db_Connect.connect();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1,title);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next())
            {
                journal.setId(resultSet.getInt("id"));
                journal.setTitle(resultSet.getString("title"));
                journal.setDescription(resultSet.getString("description"));
                journal.setPublisher(resultSet.getString("publisher"));
                journal.setImage(resultSet.getString("image"));
                journal.setRelease_Date(resultSet.getString("release_date"));
                journal.setPageCount(resultSet.getString("pagecount"));
                journal.setGenre(resultSet.getString("genre"));
                journal.setView(resultSet.getString("view_count"));
                journal.setLike(resultSet.getString("subscription_count"));
                journal.setEdition(resultSet.getString("edition"));

            }
        }
        catch (Exception ex)
        {
            System.out.println("Error while viewing book" + ex.getMessage());
            ex.printStackTrace();
        }
        finally {
            JdbcUtil.close(resultSet,preparedStatement,connection);
        }
        return journal;
    }
}
