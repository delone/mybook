package myad.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * Created by bill on 2016-01-13.
 */
public class Db_Connect {
    public static Connection connect() {
        Connection con = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");//This boy loads class into memory
            con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "myad", "myad");//this boy uses registered jdbc driver to create connection
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return con;
    }

//    public static Connection connect() {
//        Connection con = null;
//        try {
//            Context initContext = new InitialContext();
//            Context envContext = (Context) initContext.lookup("java:/comp/env/");
//            DataSource ds = (DataSource) envContext.lookup("jdbc/MyDB");
//            con = ds.getConnection();
//            con.setAutoCommit(false);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (NamingException e) {
//            e.printStackTrace();
//        }
//
//        return con;
//    }

}
