package myad.AdminPanel.journal;

import myad.Database.Database_Helper;
import myad.Model.JournalModel;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by bill on 2016-01-23.
 */
@WebServlet("/admin/journal/view")
public class ViewJournalServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        process(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        process(request,response);
    }

    public static void process(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException
    {
        if(request.getParameter("t")!=null)
        {
            Database_Helper database_helper = new Database_Helper();
            JournalModel journal = new JournalModel();
            try
            {
                journal = database_helper.viewJournal(request.getParameter("t"));//passing title getting results
                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
                if(journal.getId()!=0)
                {
                    request.setAttribute("journal",journal);
                    RequestDispatcher requestDispatcher = request.getRequestDispatcher("/admin/journal/view/index.jsp");
                    requestDispatcher.include(request,response);
                }
                else {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                }

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        else {
            response.sendRedirect("/journal/");
        }
    }
}
