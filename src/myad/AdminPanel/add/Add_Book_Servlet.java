package myad.AdminPanel.add;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * Created by bill on 2016-01-09.
 */
@WebServlet("/admin/add_book/")
@MultipartConfig(fileSizeThreshold = 1024*1024*10,
        maxFileSize = 1024*1024*20,
        maxRequestSize = 1024*1024*50)
public class Add_Book_Servlet extends HttpServlet
{
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String title = "";
        String description = "";
        String author = "";
        String publisher = "";
        String page = "";
        String edition = "";
        String release_date = "";
        ///getting values//////////
        boolean complete = false;
        title = request.getParameter("title");
        CheckData(title);
        description = request.getParameter("description");
        description.trim();
        author = request.getParameter("author");
        CheckData(author);
        publisher = request.getParameter("publisher");
        CheckData(publisher);
        page = request.getParameter("page");
        CheckData(page);
        edition = request.getParameter("edition");
        CheckData(edition);
        release_date = request.getParameter("release_date");
//        CheckData(release_date);
        try
        {
            Part part = request.getPart("filename");
            String filename = ExtractFileName(part);
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection con= DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","myad","myad");
            PreparedStatement preparedStatement = con.prepareStatement("insert into  books(id,TITLE," +
                    "DESCRIPTION,AUTHOR,PUBLISHER," +
                    "IMAGE,RELEASE_DATE,PAGECOUNT,GENRE,VIEW_COUNT,SUBSCRIPTION_COUNT,EDITION)" +
                    " VALUES(Books_ID_SEQUENCE.nextval,?,?,?,?,?,?,?,?,?,?,?)");
            preparedStatement.setString(1,title);
            preparedStatement.setString(2,description);
            preparedStatement.setString(3,author);
            preparedStatement.setString(4,publisher);
            preparedStatement.setString(5,filename);
            preparedStatement.setString(6,"12-JAN-16");
            preparedStatement.setString(7,page);
            preparedStatement.setString(8,"genre");
            preparedStatement.setString(9,"1");
            preparedStatement.setString(10,"1");
            preparedStatement.setString(11,edition);
            System.out.println(title+"  "+description+"  "+author +"  "+publisher+"  "+filename+"  "+page+"  "+edition);
            //Books_ID_SEQUENCE
//            preparedStatement.setString(7,release_date);
            int  rs = preparedStatement.executeUpdate();
            if (rs!=0)
            {
                System.out.println("Inserted you sun of a man and a woman");
                File file = new File("C:\\Users\\bill\\Downloads\\MyBook\\web\\image\\"+ filename);

                InputStream input = part.getInputStream();
                Files.copy(input, file.toPath());
                response.sendRedirect("/admin/home/index.jsp");
            }
            else
            {
                System.out.println("go and drink a cup of cold water not INSERTED");
            }

        }
        catch ( Exception e)
        {
            System.out.print(e.getMessage());
        }
    }

    private void  CheckData(String data)
    {
        data.trim();
    }



    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/admin/add_book/index.jsp");
        requestDispatcher.forward(request,response);
    }

    private  String ExtractFileName(Part part)
    {
        String content = part.getHeader("content-disposition");
        String  [] items = content.split(";");
        for (String s:items)
        {
            if(s.trim().startsWith("filename"))
            {
                return s.substring(s.indexOf("=")+2,s.length()-1);
            }
        }
        return "";
    }
}
