package myad.AdminPanel.add;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * Created by bill on 2016-01-09.
 */
@WebServlet("/admin/add_journal/")
@MultipartConfig(fileSizeThreshold = 1024*1024*10,
        maxFileSize = 1024*1024*20,
        maxRequestSize = 1024*1024*50)
public class Add_Journal_Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String title = "";
        String description = "";
        String author = "";
        String publisher = "";
        String page = "";
        String edition = "";
        String release_date = "";
        ///getting values//////////
        boolean complete = false;
        title = request.getParameter("title");
        CheckData(title);
        description = request.getParameter("description");
        description.trim();
        author = request.getParameter("author");
        CheckData(author);
        publisher = request.getParameter("publisher");
        CheckData(publisher);
        page = request.getParameter("page");
        CheckData(page);
        edition = request.getParameter("edition");
        CheckData(edition);
        release_date = request.getParameter("release_date");
//        CheckData(release_date);
        Part part = request.getPart("filename");

        try
        {System.out.println("1111");

            Class.forName("oracle.jdbc.driver.OracleDriver");
            System.out.println("1111");
            Connection con= DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","myad","myad");
            PreparedStatement preparedStatement = con.prepareStatement("insert into  JOURNALS(id,TITLE," +
                    "DESCRIPTION,PUBLISHER," +
                    "IMAGE,RELEASE_DATE,PAGECOUNT,GENRE,VIEW_COUNT,SUBSCRIPTION_COUNT,EDITION)" +
                    " VALUES(Journals_ID_SEQUENCE.nextval,?,?,?,?,?,?,?,?,?,?)");
            String filename = ExtractFileName(part);
            preparedStatement.setString(1,title);System.out.println("1111");
            preparedStatement.setString(2,description);System.out.println("1111");
            preparedStatement.setString(3,publisher);System.out.println("1111");
            preparedStatement.setString(4,filename);System.out.println("1111");
            preparedStatement.setString(5,"12-JAN-16");System.out.println("1111");
            preparedStatement.setString(6,page);System.out.println("1111");
            preparedStatement.setString(7,"genre");System.out.println("1111");
            preparedStatement.setString(8,"1");System.out.println("1111");
            preparedStatement.setString(9,"1");System.out.println("1111");
            preparedStatement.setString(10,edition);System.out.println("1111");
            System.out.println(title+"  "+description+"  "+author +"  "+publisher+"  "+filename+"  "+page+"  "+edition);
            //Books_ID_SEQUENCE
//            preparedStatement.setString(7,release_date);
            int  rs = preparedStatement.executeUpdate();System.out.println("1111");
            if (rs!=0)
            {
                System.out.println("Inserted you sun of a man and a woman");
                File file = new File("C:\\Users\\bill\\Downloads\\MyBook\\web\\image\\"+ filename);
                InputStream input = part.getInputStream();
                Files.copy(input, file.toPath());
                response.sendRedirect("/admin/home/index.jsp");
            }
            else
            {
                System.out.println("go and drink a cup of cold water not INSERTED");
            }

        }
        catch ( Exception e)
        {
            System.out.print(e.getMessage()+"jhjjfkf");
        }
    }

    private void  CheckData(String data)
    {
        data.trim();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/admin/add_journal/index.jsp");
        requestDispatcher.forward(request,response);
    }

    private  String ExtractFileName(Part part)
    {
        String content = part.getHeader("content-disposition");
        String  [] items = content.split(";");
        for (String s:items)
        {
            if(s.trim().startsWith("filename"))
            {
                return s.substring(s.indexOf("=")+2,s.length()-1);
            }
        }
        return "";
    }
}
