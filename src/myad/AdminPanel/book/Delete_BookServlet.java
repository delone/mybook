package myad.AdminPanel.book;

import myad.Database.Database_Helper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

/**
 * Created by bill on 2016-01-15.
 */
@WebServlet("/admin/delete_book")
public class Delete_BookServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Integer id;
        Integer result = 0;
        id = Integer.parseInt(request.getParameter("Id").trim());
        try {
             result = new Database_Helper().delete_Book(id);
            System.out.print("i am "+result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    }
}
