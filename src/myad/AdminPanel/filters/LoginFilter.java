package myad.AdminPanel.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by bill on 2016-01-09.
 */
public class LoginFilter implements Filter
{
    public void destroy()
    {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException
    {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        if(request.getSession().getAttribute("username")==null)
        {
            chain.doFilter(req, resp);
        }
        else
        {
            response.sendRedirect("/admin/myad.AdminPanel.home");
        }
    }

    public void init(FilterConfig config) throws ServletException
    {

    }

}
