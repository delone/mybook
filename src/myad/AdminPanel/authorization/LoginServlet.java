package myad.AdminPanel.authorization;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;

/**
 * Created by bill on 2016-01-07.
 */
@WebServlet(urlPatterns = {"/login/"})
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String error = "";
        String username = "";
        String password = "";
        username = request.getParameter("username");
        password = request.getParameter("password");
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login/index.jsp");

            try
            {

                Class.forName("oracle.jdbc.driver.OracleDriver");
                Connection con= DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","myad","myad");
                PreparedStatement preparedStatement = con.prepareStatement("select * from  admins where username = ? and password = ?");
                preparedStatement.setString(1,username);
                preparedStatement.setString(2,password);
                ResultSet rs =preparedStatement.executeQuery();
                if (rs.next())
                {
                    HttpSession session = request.getSession(true);
                    session.setMaxInactiveInterval(60);
                    session.setAttribute("username",username);
                    session.setAttribute("id",rs.findColumn("id"));
                    session.setAttribute("name",rs.findColumn("name"));
                    session.setAttribute("surname",rs.findColumn("surname"));
                    System.out.println( session.getAttribute("username").toString());
                    response.sendRedirect("/admin/home/");
                }
                else
                {
                    error = "The username or password is wrong";
                    request.setAttribute("username",username);
                    request.setAttribute("password",password);
                    request.setAttribute("error",error);
                    requestDispatcher.forward(request,response);
                }

            }
            catch ( Exception e)
            {
                System.out.print(e.getMessage());
            }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login/index.jsp");
        requestDispatcher.forward(request,response);
    }
}
