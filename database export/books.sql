--------------------------------------------------------
--  File created - Saturday-January-23-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table BOOKS
--------------------------------------------------------

  CREATE TABLE "MYAD"."BOOKS" 
   (	"ID" NUMBER(10,0), 
	"TITLE" VARCHAR2(100 BYTE), 
	"DESCRIPTION" VARCHAR2(4000 BYTE), 
	"AUTHOR" VARCHAR2(25 BYTE), 
	"PUBLISHER" VARCHAR2(25 BYTE), 
	"IMAGE" VARCHAR2(100 BYTE), 
	"RELEASE_DATE" DATE, 
	"PAGECOUNT" NUMBER(4,0), 
	"GENRE" VARCHAR2(30 BYTE), 
	"VIEW_COUNT" NUMBER(10,0), 
	"SUBSCRIPTION_COUNT" NUMBER(6,0), 
	"EDITION" CHAR(8 BYTE), 
	"B" CHAR(1 BYTE) DEFAULT 'b'
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into MYAD.BOOKS
SET DEFINE OFF;
Insert into MYAD.BOOKS (ID,TITLE,DESCRIPTION,AUTHOR,PUBLISHER,IMAGE,RELEASE_DATE,PAGECOUNT,GENRE,VIEW_COUNT,SUBSCRIPTION_COUNT,EDITION,B) values (108,'The Web','Tim Burners LeeTim Burners LeeTim Burners LeeTim Burners Lee','Tim Burners Lee','Tim Burners Lee','dfgdfg.jpg',to_date('12-JAN-16','DD-MON-RR'),345,'genre',1,1,'3       ','b');
Insert into MYAD.BOOKS (ID,TITLE,DESCRIPTION,AUTHOR,PUBLISHER,IMAGE,RELEASE_DATE,PAGECOUNT,GENRE,VIEW_COUNT,SUBSCRIPTION_COUNT,EDITION,B) values (110,'The Life of Pi','The Life of PiThe Life of PiThe Life of PiThe Life of Pi','The Life of Pi','The Life of Pi','download.jpg',to_date('12-JAN-16','DD-MON-RR'),234,'genre',1,1,'54      ','b');
Insert into MYAD.BOOKS (ID,TITLE,DESCRIPTION,AUTHOR,PUBLISHER,IMAGE,RELEASE_DATE,PAGECOUNT,GENRE,VIEW_COUNT,SUBSCRIPTION_COUNT,EDITION,B) values (112,'The Life of Pi','The Life of PiThe Life of PiThe Life of PiThe Life of Pi','The Life of Pi','The Life of Pi','download.jpg',to_date('12-JAN-16','DD-MON-RR'),234,'genre',1,1,'54      ','b');
Insert into MYAD.BOOKS (ID,TITLE,DESCRIPTION,AUTHOR,PUBLISHER,IMAGE,RELEASE_DATE,PAGECOUNT,GENRE,VIEW_COUNT,SUBSCRIPTION_COUNT,EDITION,B) values (114,'Di Caprio','Lenoardo Di CaprioDi CaprioDi Caprio','Di CaprioDi Caprio','Di Caprio','fdgfdgfd.jpg',to_date('12-JAN-16','DD-MON-RR'),876,'genre',1,1,'6       ','b');
Insert into MYAD.BOOKS (ID,TITLE,DESCRIPTION,AUTHOR,PUBLISHER,IMAGE,RELEASE_DATE,PAGECOUNT,GENRE,VIEW_COUNT,SUBSCRIPTION_COUNT,EDITION,B) values (118,'Java','JavaJavaJavaJava','JavaJavaJava','JavaJava','mnbmnb.jpg',to_date('12-JAN-16','DD-MON-RR'),544,'genre',1,1,'1       ','b');
Insert into MYAD.BOOKS (ID,TITLE,DESCRIPTION,AUTHOR,PUBLISHER,IMAGE,RELEASE_DATE,PAGECOUNT,GENRE,VIEW_COUNT,SUBSCRIPTION_COUNT,EDITION,B) values (104,'The Complete Reference Java','This is about JavaThis is about JavaThis is about JavaThis is about JavaThis is about Java','Mecnun Abdurahmanov','O''Reilly','bnmnbmbn.jpg',to_date('12-JAN-16','DD-MON-RR'),876,'genre',1,1,'1       ','b');
Insert into MYAD.BOOKS (ID,TITLE,DESCRIPTION,AUTHOR,PUBLISHER,IMAGE,RELEASE_DATE,PAGECOUNT,GENRE,VIEW_COUNT,SUBSCRIPTION_COUNT,EDITION,B) values (106,'Harry Potter','Harry Potter is good boyHarry Potter is good boyHarry Potter is good boy','J.K Rowling','Publisher','dasdasd.jpg',to_date('12-JAN-16','DD-MON-RR'),456,'genre',1,1,'3       ','b');
--------------------------------------------------------
--  DDL for Index PK_ID_ITEMS
--------------------------------------------------------

  CREATE UNIQUE INDEX "MYAD"."PK_ID_ITEMS" ON "MYAD"."BOOKS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table BOOKS
--------------------------------------------------------

  ALTER TABLE "MYAD"."BOOKS" ADD CONSTRAINT "PK_ID_ITEMS" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "MYAD"."BOOKS" MODIFY ("IMAGE" NOT NULL ENABLE);
  ALTER TABLE "MYAD"."BOOKS" MODIFY ("AUTHOR" NOT NULL ENABLE);
  ALTER TABLE "MYAD"."BOOKS" MODIFY ("TITLE" NOT NULL ENABLE);
--------------------------------------------------------
--  DDL for Trigger BOOKS_ID_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MYAD"."BOOKS_ID_TRIGGER" 
before insert on books
declare id number;
BEGIN
id:=BOOKS_ID_SEQUENCE.nextval;
END;
/
ALTER TRIGGER "MYAD"."BOOKS_ID_TRIGGER" ENABLE;
