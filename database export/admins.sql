--------------------------------------------------------
--  File created - Saturday-January-23-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ADMINS
--------------------------------------------------------

  CREATE TABLE "MYAD"."ADMINS" 
   (	"ID" NUMBER(10,0) DEFAULT 4, 
	"PASSWORD" VARCHAR2(17 BYTE), 
	"NAME" VARCHAR2(20 BYTE), 
	"SURNAME" VARCHAR2(25 BYTE), 
	"EMAIL" VARCHAR2(50 BYTE), 
	"USERNAME" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into MYAD.ADMINS
SET DEFINE OFF;
Insert into MYAD.ADMINS (ID,PASSWORD,NAME,SURNAME,EMAIL,USERNAME) values (1,'localhost','Admin','Adminzade','mabdurahmanov@az','localhost');
Insert into MYAD.ADMINS (ID,PASSWORD,NAME,SURNAME,EMAIL,USERNAME) values (10,'sda','Admasdain','Admasdinzade','mabdurahmanov@std.qu.edu.az','localdsahost');
--------------------------------------------------------
--  DDL for Index PK_USERS_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "MYAD"."PK_USERS_ID" ON "MYAD"."ADMINS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table ADMINS
--------------------------------------------------------

  ALTER TABLE "MYAD"."ADMINS" ADD CONSTRAINT "ADMINS_CHECK_EMAIL1" CHECK ( email like('%@%')) ENABLE;
  ALTER TABLE "MYAD"."ADMINS" ADD CONSTRAINT "PK_USERS_ID" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  DDL for Trigger ADMINS_ID_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "MYAD"."ADMINS_ID_TRIGGER" 
before insert on admins
declare id number;
BEGIN
id:=ADMINS_ID_SEQUENCE.nextval;
END;
/
ALTER TRIGGER "MYAD"."ADMINS_ID_TRIGGER" ENABLE;
