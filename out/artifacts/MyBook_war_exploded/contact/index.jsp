<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.sql.ResultSet" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <%--<link rel="icon" href="http://www.codexworld.com/wp-content/uploads/2014/09/favicon.ico" type="image/x-icon" />--%>
    <meta name="keywords" content="mybook,books,buy,sell,advertisement of books">
    <meta name="author" content="MyBook">
    <title>mybook </title>
    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <!-- Add custom CSS here -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <script src="http://demos.codexworld.com/includes/js/bootstrap.js"></script>
    <style type="text/css">
        .row{ margin:20px 20px 20px 20px;}
        .ratings{ font-size:25px !important;}
        .thumbnail img {
            width: 100%;
        }

        .ratings {
            padding-right: 10px;
            padding-left: 10px;
            color: #d17581;
        }

        .thumbnail {
            padding: 0;
        }

        .thumbnail .caption-full {
            padding: 9px;
            color: #333;
        }
        .glyphicon-thumbs-up:hover{ color:#008000; cursor:pointer;}
        .glyphicon-thumbs-down:hover{ color: #E10000; cursor:pointer;}
        .counter{ color:#333333;}
        .thumbnail img{height:200px;}
    </style>

    <script type="text/javascript" src="/assets/js/jquery.js"></script>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li >
                    <a  href="/" >Home</a>
                </li>
                <li>
                    <a href="/book/">Books</a>
                </li>
                <li>
                    <a href="/journal/">Journals</a>
                </li>
                <li>
                    <a style="color: #ffffff" href="/contact/">Contact</a>
                </li>
                <li>
                    <a href="/about/">About</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
<div class="bar-header"></div>
<div class="container">


</div>






<div class="bar-footer"></div>

</body>
</html>
