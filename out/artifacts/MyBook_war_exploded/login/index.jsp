<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="false"%>
<html>
  <head>
    <title>Login</title>
    <link rel="stylesheet" href="/assets/css/login.css">
  </head>
  <body>

  <div>
  <form action="/login/" method="post">
    <%
      if(null!=request.getAttribute("error"))
      {
        out.print("<span style=\"color:red;\">");
        out.print(request.getAttribute("error"));
        out.print("</span>");
      }
    %>
    <br>
    <br>
    Username:<br>
    <input name="username" type="text" value="<%if(null!=request.getAttribute("username")){out.print(request.getAttribute("username"));}%>"><br>
    Password:<br>
    <input type="password" name="password" value="<%if(null!=request.getAttribute("password")) { out.print(request.getAttribute("password")); }%>"><br>
    <input type="submit" value="Sign in">
  </form>
  </div>
  </body>
</html>