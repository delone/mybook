<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <%--<link rel="icon" href="http://www.codexworld.com/wp-content/uploads/2014/09/favicon.ico" type="image/x-icon" />--%>
    <meta name="keywords" content="mybook,books,buy,sell,advertisement of books">
    <meta name="author" content="MyBook">
    <title>mybook </title>
    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <!-- Add custom CSS here -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <style type="text/css">
        .row{ margin:20px 20px 20px 20px;}
        .ratings{ font-size:25px !important;}
        .thumbnail img {
            width: 100%;
        }

        .ratings {
            padding-right: 10px;
            padding-left: 10px;
            color: #d17581;
        }

        .thumbnail {
            padding: 0;
        }

        .thumbnail .caption-full {
            padding: 9px;
            color: #333;
        }
        .glyphicon-thumbs-up:hover{ color:#008000; cursor:pointer;}
        .glyphicon-thumbs-down:hover{ color: #E10000; cursor:pointer;}
        .counter{ color:#333333;}
        .thumbnail img{height:200px;}

        .book_profile{
            float: left;

        }

        .book_info {
            float: left;
            padding-left:  50px;
            padding-right: 50px;
            max-width: 100ch;
        }



        .description{
            float: left;
            clear: left;
            padding: 20px;
        }
    </style>

    <script type="text/javascript" src="/assets/js/jquery.js"></script>
    <script type="text/javascript">
        function cwRating(id,type,target){
            $.ajax({
                type:'POST',
                url:'rating.php',
                data:'id='+id+'&type='+type,
                success:function(msg){
                    if(msg == 'err'){
                        alert('Some problem occured, please try again.');
                    }else{
                        $('#'+target).html(msg);
                    }
                }
            });
        }
    </script>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <%--<a class="navbar-brand" href="http://www.codexworld.com/">--%>
            <%--<img src="http://www.codexworld.com/wp-content/uploads/2014/09/codexworld-logo.png" alt="CodexWorld">--%>
            <%--</a>--%>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="/admin/home/">Home</a>
                </li>
                <li>
                    <a style="color: #ffffff" href="/admin/book/">Books</a>
                </li>
                <li>
                    <a href="/admin/add_book/">Add Book</a>
                </li>
                <li>
                    <a  href="/admin/journal/">Journals</a>
                </li>
                <li>
                    <a href="/admin/add_journal/">Add Journal</a>
                </li>
                <li>
                    <a href="/admin/profile/">Profile</a>
                </li>
                <li>
                    <a href="/logout/">Logout</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
<div class="bar-header"></div>
<div class="container">

    <div class="row" >
        <button type="button" class="btn btn-success" >Edit</button>
        <!-- Profile -->
        <div class="book_profile">
            <img src="/image/${book.image}" alt=""  border="solid" width="500px" height="380px">


        </div>


        <!-- Info -->
        <div class="book_info">
            <h1>${book.title}</h1>

            <h3>Author: ${book.author}</h3>
            <h3>Publisher:  ${book.publisher}</h3>
            <h3>Genre:  ${book.genre}</h3>
            <h3>Edition:  ${book.edition}</h3>
            <h3>PageCount:</h3>
            <h3>Release Date:</h3>
        </div>



        <!-- Event Text -->
        <div class="description" >
            <h4><strong>Description:</strong> ${book.description}</h4>
        </div>
    </div>

</div>



<div class="bar-footer"></div>
<script src="http://demos.codexworld.com/includes/js/bootstrap.js"></script>

</body>
</html>
