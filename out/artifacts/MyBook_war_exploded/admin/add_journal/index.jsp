
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <%--<link rel="icon" href="http://www.codexworld.com/wp-content/uploads/2014/09/favicon.ico" type="image/x-icon" />--%>
    <meta name="keywords" content="mybook,books,buy,sell,advertisement of books">
    <meta name="author" content="MyBook">
    <title>mybook </title>
    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <!-- Add custom CSS here -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <style type="text/css">
        .row{ margin:20px 20px 20px 20px;}
        .ratings{ font-size:25px !important;}
        .thumbnail img {
            width: 100%;
        }

        .ratings {
            padding-right: 10px;
            padding-left: 10px;
            color: #d17581;
        }

        .thumbnail {
            padding: 0;
        }

        .thumbnail .caption-full {
            padding: 9px;
            color: #333;
        }
        .glyphicon-thumbs-up:hover{ color:#008000; cursor:pointer;}
        .glyphicon-thumbs-down:hover{ color: #E10000; cursor:pointer;}
        .counter{ color:#333333;}
        .thumbnail img{height:200px;}




        input[type=text], select {
        width: 50%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
        }

        textarea
        {
        width: 49%;
        border-radius: 4px;
        border: 1px solid #ccc;
        max-height: inherit;
        }


        input[type=submit] {
        width: 50%;
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        }

        input[type=submit]:hover {
        background-color: #45a049;
        }

        input[type=text]:focus {
        background-color: lightblue;
        }
        textarea:focus
        {
        background-color: lightblue;
        }


        .error
        {
        color: rgb(255,0,0);

        }
        .success
        {
        color:  #4CAF50;
        }

        .myform {
        border-radius: 5px;
        background-color: #f0f6f1;
        padding: 30px;
        margin-left: 380px;
        margin-right: 350px;
        max-width: 800px;
        max-height: 800px;
        margin-top: 100px;
        margin-bottom: 50px;
        }
    </style>

    <script type="text/javascript" src="/assets/js/jquery.js"></script>
    <script type="text/javascript">
        function cwRating(id,type,target){
            $.ajax({
                type:'POST',
                url:'rating.php',
                data:'id='+id+'&type='+type,
                success:function(msg){
                    if(msg == 'err'){
                        alert('Some problem occured, please try again.');
                    }else{
                        $('#'+target).html(msg);
                    }
                }
            });
        }
    </script>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <%--<a class="navbar-brand" href="http://www.codexworld.com/">--%>
                <%--<img src="http://www.codexworld.com/wp-content/uploads/2014/09/codexworld-logo.png" alt="CodexWorld">--%>
            <%--</a>--%>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="/admin/home/">Home</a>
                </li>
                <li>
                    <a href="/admin/book/">Books</a>
                </li>
                <li>
                    <a href="/admin/add_book/">Add Book</a>
                </li>
                <li>
                    <a href="/admin/journal/">Journals</a>
                </li>
                <li>
                    <a style="color: #ffffff" href="/admin/add_journal/">Add Journal</a>
                </li>
                <li>
                    <a href="/admin/profile/">Profile</a>
                </li>
                <li>
                    <a href="/logout/">Logout</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
<div class="bar-header"></div>

    <div class="myform">

        <form action="/admin/add_journal/" enctype="multipart/form-data" method="post">
            Title:<br>
            <input type="text" name="title"><br>
            Description:<br>
            <textarea name="description"></textarea><br>
            Author:<br>
            <input name="author" type="text"><br>
            Publisher:<br>
            <input name="publisher" type="text"><br>
            Page:<br>
            <input name="page" type="text"><br>
            Edition:<br>
            <input name="edition" type="text"><br>
            Release Date:<br>
            <input type="text" name="name"><br>
            Image:<br>
            <input type="file" name="filename"><br>
            <br>
            <input type="submit" value="Add">

        </form>

    </div>
<div class="bar-footer"></div>
<script src="http://demos.codexworld.com/includes/js/bootstrap.js"></script>


</body>
</html>
